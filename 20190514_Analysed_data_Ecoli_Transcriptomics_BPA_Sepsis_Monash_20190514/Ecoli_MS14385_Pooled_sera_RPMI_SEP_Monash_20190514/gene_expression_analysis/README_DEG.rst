README
######

No significant DEG were shown with voom (refer to visual reports).

EdgeR results are provided here as well for reference, but note that voom generally performs better in the case of noisy heterogenous data.

- `Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_20190514.txt`
  - Stores the voom-adjusted values for all gene *names*
  - This went through the same pipeline for all other Sepsis samples and is used to generate the interactive glimma reports (gene names used for informativeness)
- `Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_WITH_VOOM_GENEID_20190514.txt`
  - Stores the voom-adjusted values for all gene *ids*
  - Here we use gene ids for completeness
- `Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_WITHOUT_VOOM_GENEID_20190514.txt`
  - Stores the NON voom-adjusted values for all gene *ids*
  - Here we use gene ids for completeness

Comparing the two tables above for WITH VOOM vs WITHOUT VOOM, we can see that there is a large difference in the range of adjusted p-values. WITH VOOM has no significant p values shown, while WITHOUT VOOM does.

::
  Files in directory:
  ├── Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_20190514.txt
  ├── Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_WITHOUT_VOOM_GENEID_20190514.txt
  ├── Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_WITH_VOOM_GENEID_20190514.txt
  ├── Ecoli_MS14385_Pooled_sera_RPMI_Gene_id_20190514.txt
  ├── Ecoli_MS14385_Pooled_sera_RPMI_Gene_name_20190514.txt
  └── README_DEG.rst
