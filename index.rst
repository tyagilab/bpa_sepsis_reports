#######################################
Transcriptomics Analysis Summary Report
#######################################

.. note:: This document was last updated on 26/08/2019. Data for *E coli* strain ```MS14387``` and *S pyogenes* strains ```5448```, ```HKU419```, ```SP444``` were added.

.. footer:: ###Page###

.. contents::

.. note:: If you are experiencing difficulties with clicking links in the ```.pdf``` version of this report on a Mac, please use ```Adobe Reader``` to open the file instead of the default ```Preview```.

Summary
#######

This report contains a summary of computational methods used to generate the resulting interactive visual reports from the raw sequence data. Please contact `Tyrone Chen`_ or `Sonika Tyagi`_ for more details on the computational workflow. Interactive visual reports, the lists of differentially expressed genes and count tables are included. Details on the full directory structure are included at the end of this document for reference.

.. _Tyrone Chen: mailto:tyrone.chen@monash.edu
.. _Sonika Tyagi: mailto:sonika.tyagi@monash.edu

Methods
#######

Workflow
========

Fastq files were aligned to publicly available reference genomes and their corresponding annotations (available through NCBI). Reads were then quantified. To obtain differentially expressed gene tables and glimma plots, samples were first filtered on a CPM value ranging from 0.5-1 (approximately equal to 10 counts). Sample groups (eg Pooled sera, RPMI+glucose) which had at least 75% of samples meeting this criteria for a single gene are included in the DEG, and genes not meeting this criteria were excluded from the analysis.

.. note:: The full count tables with all genes are available.

Software versions and parameters
================================

.. write in paragraph form also for manuscript

Genome assemblies and annotations were obtained from their corresponding NCBI accessions. Genome annotation `gff` files were converted to `gtf` files using ```cufflinks 2.2.1```. A genome index was built for each strain using ```bwa 0.7.12```. Reads were aligned to the genome using ```bwa 0.7.12``` with default settings to generate bam files. After sorting these bam files with ```samtools 1.9```, reads were quantified into count tables using ```featureCounts``` from ```subread 1.5.1```. Differentially expressed gene tables were generated using ```edgeR 3.16.5``` and ```limma 3.30.13``` in ```R 3.2.2```. A voom transform was also applied using ```edgeR 3.16.5```. Interactive plots were generated with ```Glimma 1.2.1``` and ```limma 3.30.13``` in ```R 3.2.2```. Wrappers in the bioinformatics pipeline ```s4m 0.8``` was used to streamline generation of differentially expressed gene tables and glimma plots.

Interactive reports
###################

.. include count tables!

All volcano and mean-difference plots show ```Pooled sera``` vs ```RPMI``` samples except where the naming convention referred to in the table changes to reflect this (eg ```Spneumoniae_180-2_glucose-galactose_volcano``` would show ```glucose``` vs ```galactose``` samples). This is because *Streptococcus pneumoniae* has three comparison groups while other species have just two.

.. note:: Opening many plots simultaneously can cause issues. If you experience a slow browser, please only open a few interactive reports at a time for best results. This is because every detected gene is loaded (~5000 per plot)!

.. interactive reports
.. _Ecoli_MS14387_volcano: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14387_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Ecoli_MS14387_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190824/volcano.html
.. _Ecoli_MS14387_md-plot: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14387_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Ecoli_MS14387_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190824/MD-Plot.html
.. _Ecoli_MS14385_volcano: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14385_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_MS14385_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Ecoli_MS14385_md-plot: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14385_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_MS14385_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Ecoli_MS14386_volcano: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14386_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_MS14386_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Ecoli_MS14386_md-plot: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14386_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_MS14386_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Ecoli_MS14384_volcano: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14384_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_MS14384_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Ecoli_MS14384_md-plot: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14384_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_MS14384_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Ecoli_B36_volcano: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_B36_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_B36_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Ecoli_B36_md-plot: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_B36_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Ecoli_B36_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Kvariicola_AJ055_volcano: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_AJ055_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_AJ055_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Kvariicola_AJ055_md-plot: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_AJ055_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_AJ055_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Kvariicola_AJ292_volcano: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_AJ292_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_AJ292_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Kvariicola_AJ292_md-plot: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_AJ292_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_AJ292_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Kvariicola_03-311-0071_volcano: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_03-311-0071_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_03-311-0071_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Kvariicola_03-311-0071_md-plot: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_03-311-0071_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_03-311-0071_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Kvariicola_04153260899A_volcano: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_04153260899A_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_04153260899A_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Kvariicola_04153260899A_md-plot: ./20190514_Analysed_data_Kvariicola_Transcriptomics_BPA_Sepsis_Monash_20190514/Kvariicola_04153260899A_Transcriptomics_BPA_Sepsis_Monash_20190514/visual_reports/Kvariicola_04153260899A_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Saureus_BPH2760_volcano: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2760_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2760_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Saureus_BPH2760_md-plot: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2760_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2760_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Saureus_BPH2947_volcano: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2947_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2947_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Saureus_BPH2947_md-plot: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2947_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2947_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Saureus_BPH2986_volcano: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2986_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2986_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Saureus_BPH2986_md-plot: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2986_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2986_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Saureus_BPH2819_volcano: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2819_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2819_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Saureus_BPH2819_md-plot: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2819_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2819_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Saureus_BPH2900_volcano: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2900_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2900_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Saureus_BPH2900_md-plot: ./20190514_Analysed_data_Saureus_Transcriptomics_BPA_Sepsis_Monash_20190514/Saureus_BPH2900_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Saureus_BPH2900_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spyogenes_5448_volcano: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_5448_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Spyogenes_5448_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_20190824/volcano.html
.. _Spyogenes_5448_md-plot: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_5448_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Spyogenes_5448_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_20190824/MD-Plot.html
.. _Spyogenes_HKU419_volcano: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_HKU419_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Spyogenes_HKU419_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190824/volcano.html
.. _Spyogenes_HKU419_md-plot: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_HKU419_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Spyogenes_HKU419_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190824/MD-Plot.html
.. _Spyogenes_SP444_volcano: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_SP444_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Spyogenes_SP444_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_20190824/volcano.html
.. _Spyogenes_SP444_md-plot: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_SP444_Pooled_sera_RPMI_SEP_Monash_20190824/visual_reports/Spyogenes_SP444_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_20190824/MD-Plot.html
.. _Spyogenes_PS003_volcano: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_PS003_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Spyogenes_PS003_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Spyogenes_PS003_md-plot: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_PS003_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Spyogenes_PS003_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spyogenes_PS006_volcano: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_PS006_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Spyogenes_PS006_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Spyogenes_PS006_md-plot: ./20190514_Analysed_data_Spyogenes_Transcriptomics_BPA_Sepsis_Monash_20190514/Spyogenes_PS006_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Spyogenes_PS006_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_4496_sera-glucose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4496_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_4496_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_4496_sera-glucose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4496_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_4496_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_4496_sera-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4496_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_4496_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_4496_sera-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4496_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_4496_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_4496_glucose-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4496_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_4496_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_4496_glucose-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4496_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_4496_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_4559_sera-glucose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4559_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190704/visual_reports/Spneumoniae_4559_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190704/volcano.html
.. _Spneumoniae_4559_sera-glucose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4559_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190704/visual_reports/Spneumoniae_4559_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190704/MD-Plot.html
.. _Spneumoniae_4559_sera-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4559_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190704/visual_reports/Spneumoniae_4559_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190704/volcano.html
.. _Spneumoniae_4559_sera-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4559_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190704/visual_reports/Spneumoniae_4559_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190704/MD-Plot.html
.. _Spneumoniae_4559_glucose-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4559_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190704/visual_reports/Spneumoniae_4559_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190704/volcano.html
.. _Spneumoniae_4559_glucose-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_4559_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190704/visual_reports/Spneumoniae_4559_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190704/MD-Plot.html
.. _Spneumoniae_180-2_sera-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_2_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_2_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_180-2_sera-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_2_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_2_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_180-2_sera-glucose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_2_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_2_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_180-2_sera-glucose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_2_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_2_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_180-2_glucose-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_2_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_2_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_180-2_glucose-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_2_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_2_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_180-15_sera-glucose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_15_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_15_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_180-15_sera-glucose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_15_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_15_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_180-15_glucose-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_15_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_15_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_180-15_glucose-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_15_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_15_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_180-15_sera-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_15_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_15_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_180-15_sera-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_180_15_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_180_15_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_947_sera-glucose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_947_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_947_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_947_sera-glucose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_947_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_947_DEG_Pooled_sera_vs_RPMI_glucose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_947_glucose-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_947_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_947_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_947_glucose-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_947_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_947_DEG_RPMI_glucose_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Spneumoniae_947_sera-galactose_volcano: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_947_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_947_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/volcano.html
.. _Spneumoniae_947_sera-galactose_md-plot: ./20190514_Analysed_data_Spneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Spneumoniae_947_Pooled_sera_RPMI_glucose_RPMI_galactose_SEP_Monash_20190514/visual_reports/Spneumoniae_947_DEG_Pooled_sera_vs_RPMI_galactose_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Kpneumoniae_KPC2_volcano: ./20190514_Analysed_data_Kpneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Kpneumoniae_KPC2_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Kpneumoniae_KPC2_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Kpneumoniae_KPC2_md-plot: ./20190514_Analysed_data_Kpneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Kpneumoniae_KPC2_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Kpneumoniae_KPC2_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html
.. _Kpneumoniae_AJ218_volcano: ./20190514_Analysed_data_Kpneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Kpneumoniae_AJ218_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Kpneumoniae_AJ218_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/volcano.html
.. _Kpneumoniae_AJ218_md-plot: ./20190514_Analysed_data_Kpneumoniae_Transcriptomics_BPA_Sepsis_Monash_20190514/Kpneumoniae_AJ218_Pooled_sera_RPMI_SEP_Monash_20190514/visual_reports/Kpneumoniae_AJ218_DEG_RPMI_vs_Pooled_sera_cpmfilterminprop0.75_20190514/MD-Plot.html

.. genome assemblies

.. _4496_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/545/GCF_900618545.1_4496/GCF_900618545.1_4496_genomic.fna.gz
.. _4559_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/505/GCF_900622505.1_4559/GCF_900622505.1_4559_genomic.fna.gz
.. _947_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/555/GCF_900618555.1_947/GCF_900618555.1_947_genomic.fna.gz
.. _180-2_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/575/GCF_900618575.1_180-2/GCF_900618575.1_180-2_genomic.fna.gz
.. _180-15_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/585/GCF_900618585.1_180-15/GCF_900618585.1_180-15_genomic.fna.gz
.. _03-311-0071_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/625/GCF_900622625.1_03-311-0071/GCF_900622625.1_03-311-0071_genomic.fna.gz
.. _04153260899A_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/615/GCF_900622615.1_04153260899A/GCF_900622615.1_04153260899A_genomic.fna.gz
.. _5448_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/585/GCF_900619585.1_5448/GCF_900619585.1_5448_genomic.fna.gz
.. _AJ055_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/585/GCF_900622585.1_AJ055/GCF_900622585.1_AJ055_genomic.fna.gz
.. _AJ218_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/605/GCF_900622605.1_AJ218/GCF_900622605.1_AJ218_genomic.fna.gz
.. _AJ292_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/595/GCF_900622595.1_AJ292/GCF_900622595.1_AJ292_genomic.fna.gz
.. _B36_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/635/GCF_900622635.1_B36/GCF_900622635.1_B36_genomic.fna.gz
.. _B36_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/635/GCF_900622635.1_B36/GCF_900622635.1_B36_genomic.fna.gz
.. _BPH2760_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/215/GCF_900620215.1_BPH2760/GCF_900620215.1_BPH2760_genomic.fna.gz
.. _BPH2819_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/225/GCF_900620225.1_BPH2819/GCF_900620225.1_BPH2819_genomic.fna.gz
.. _BPH2900_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/235/GCF_900620235.1_BPH2900/GCF_900620235.1_BPH2900_genomic.fna.gz
.. _BPH2947_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/245/GCF_900620245.1_BPH2947/GCF_900620245.1_BPH2947_genomic.fna.gz
.. _BPH2986_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/255/GCF_900620255.1_BPH2986/GCF_900620255.1_BPH2986_genomic.fna.gz
.. _HKU419_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/605/GCF_900619605.1_HKU419/GCF_900619605.1_HKU419_genomic.fna.gz
.. _KPC2_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/645/GCF_900622645.1_KPC2/GCF_900622645.1_KPC2_genomic.fna.gz
.. _MS14384_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/695/GCF_900622695.1_MS14384/GCF_900622695.1_MS14384_genomic.fna.gz
.. _MS14385_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/655/GCF_900622655.1_MS14385/GCF_900622655.1_MS14385_genomic.fna.gz
.. _MS14386_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/665/GCF_900622665.1_MS14386/GCF_900622665.1_MS14386_genomic.fna.gz
.. _MS14387_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/685/GCF_900622685.1_MS14387/GCF_900622685.1_MS14387_genomic.fna.gz
.. _PS003_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/615/GCF_900619615.1_PS003/GCF_900619615.1_PS003_genomic.fna.gz
.. _PS006_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/625/GCF_900619625.1_PS006/GCF_900619625.1_PS006_genomic.fna.gz
.. _SP444_assembly: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/595/GCF_900619595.1_SP444/GCF_900619595.1_SP444_genomic.fna.gz

.. genome annotations

.. _4496_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/545/GCF_900618545.1_4496/GCF_900618545.1_4496_genomic.gff.gz
.. _4559_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/505/GCF_900622505.1_4559/GCF_900622505.1_4559_genomic.gff.gz
.. _947_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/555/GCF_900618555.1_947/GCF_900618555.1_947_genomic.gff.gz
.. _180-2_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/575/GCF_900618575.1_180-2/GCF_900618575.1_180-2_genomic.gff.gz
.. _180-15_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/618/585/GCF_900618585.1_180-15/GCF_900618585.1_180-15_genomic.gff.gz
.. _03-311-0071_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/625/GCF_900622625.1_03-311-0071/GCF_900622625.1_03-311-0071_genomic.gff.gz
.. _04153260899A_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/615/GCF_900622615.1_04153260899A/GCF_900622615.1_04153260899A_genomic.gff.gz
.. _5448_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/585/GCF_900619585.1_5448/GCF_900619585.1_5448_genomic.gff.gz
.. _AJ055_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/585/GCF_900622585.1_AJ055/GCF_900622585.1_AJ055_genomic.gff.gz
.. _AJ218_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/605/GCF_900622605.1_AJ218/GCF_900622605.1_AJ218_genomic.gff.gz
.. _AJ292_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/595/GCF_900622595.1_AJ292/GCF_900622595.1_AJ292_genomic.gff.gz
.. _B36_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/635/GCF_900622635.1_B36/GCF_900622635.1_B36_genomic.gff.gz
.. _B36_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/635/GCF_900622635.1_B36/GCF_900622635.1_B36_genomic.gff.gz
.. _BPH2760_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/215/GCF_900620215.1_BPH2760/GCF_900620215.1_BPH2760_genomic.gff.gz
.. _BPH2819_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/225/GCF_900620225.1_BPH2819/GCF_900620225.1_BPH2819_genomic.gff.gz
.. _BPH2900_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/235/GCF_900620235.1_BPH2900/GCF_900620235.1_BPH2900_genomic.gff.gz
.. _BPH2947_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/245/GCF_900620245.1_BPH2947/GCF_900620245.1_BPH2947_genomic.gff.gz
.. _BPH2986_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/620/255/GCF_900620255.1_BPH2986/GCF_900620255.1_BPH2986_genomic.gff.gz
.. _HKU419_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/605/GCF_900619605.1_HKU419/GCF_900619605.1_HKU419_genomic.gff.gz
.. _KPC2_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/645/GCF_900622645.1_KPC2/GCF_900622645.1_KPC2_genomic.gff.gz
.. _MS14384_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/695/GCF_900622695.1_MS14384/GCF_900622695.1_MS14384_genomic.gff.gz
.. _MS14385_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/655/GCF_900622655.1_MS14385/GCF_900622655.1_MS14385_genomic.gff.gz
.. _MS14386_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/665/GCF_900622665.1_MS14386/GCF_900622665.1_MS14386_genomic.gff.gz
.. _MS14387_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/685/GCF_900622685.1_MS14387/GCF_900622685.1_MS14387_genomic.gff.gz
.. _PS003_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/615/GCF_900619615.1_PS003/GCF_900619615.1_PS003_genomic.gff.gz
.. _PS006_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/625/GCF_900619625.1_PS006/GCF_900619625.1_PS006_genomic.gff.gz
.. _SP444_annotations: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/619/595/GCF_900619595.1_SP444/GCF_900619595.1_SP444_genomic.gff.gz


.. csv-table:: Links to external genome information in NCBI and internal interactive reports
  :widths: 20 20 20 20 20 20
  :header: "Species", "Strain", "Genome assemblies", "Genome annotations", "Volcano plot", "Mean-Difference plot"
  :file: reports.csv

.. _without voom: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14385_Pooled_sera_RPMI_SEP_Monash_20190514/gene_expression_analysis/Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_WITHOUT_VOOM_GENEID_20190514.txt
.. _with voom: ./20190514_Analysed_data_Ecoli_Transcriptomics_BPA_Sepsis_Monash_20190514/Ecoli_MS14385_Pooled_sera_RPMI_SEP_Monash_20190514/gene_expression_analysis/Ecoli_MS14385_DEG_Pooled_sera_vs_RPMI_cpmfilterminprop0.75_WITH_VOOM_GENEID_20190514.txt

.. note:: For the case of *Escherichia coli* strain ```MS14385```, no significant differentially expressed genes were found in comparison to the previous version of the data. This is a result of a change in analysis methods, where in the most recent version of the data ```voom``` weighting is applied in all cases to normalise samples. Compare the results of the differential expression analysis `without voom`_ and `with voom`_.


.. +--------------------+------------------------+-----------------------+------------------------+
.. | Species            | Strain                 | Volcano plot          | Mean-Difference plot   |
.. +====================+========================+=======================+========================+
.. | E coli             | B36                    | `Ecoli_B36_volcano`_  | `Ecoli_B36_md-plot`_   |
.. +--------------------+------------------------+-----------------------+------------------------+



Directory structure
###################

The following tree diagram shows the organisation of the directory structure if the user would like to access individual files. Two exceptions exist, **(1)** for the case of *Streptococcus pneumoniae*, multiple reports and tables for differentially expressed genes exist because three sample groups are assessed instead of two in all other samples. **(2)** for the case of *Escherichia coli* strain *MS14385*, two additional versions of the differentially expressed gene table is available with the identifiers ```WITH_VOOM``` and ```WITHOUT_VOOM``` within their file names. These two files are included *for reference only* to illustrate that using ```edgeR``` alone without applying ```voom``` weights may have increased the probability of obtaining false positives.

When calculating cpm values, the cpm function within edgeR with default settings was used. Note that this function adds a small pseudocount to avoid logging a number <= 0. Full details of this method is within ```edgeR``` documentation.

::

  # all values enclosed in {} will differ based on the bacterial species, strain and DEG comparison groups

  {species}_{strain}_Pooled_sera_RPMI_SEP_Monash_20190514/  # all information for this strain is here
  ├── gene_expression_analysis
  │   ├── {species}_{strain}_DEG_{group1}_vs_{group2}_cpmfilterminprop0.75_20190514.txt  # list of differentially expressed genes
  │   ├── {species}_{strain}_Pooled_sera_RPMI_Gene_id_20190514.txt  # count table, gene id as row index
  │   ├── {species}_{strain}_Pooled_sera_RPMI_Gene_name_20190514.txt  # count table, gene name (if annotated) as row index
  │   ├── cpm_GENEID.txt  # cpm values calculated with edgeR, gene id as row index
  │   ├── cpm_GENEID_log2.txt  # cpm values calculated with edgeR, gene id as row index
  │   ├── cpm_GENENAME.txt  # cpm values calculated with edgeR, gene id as row index
  │   └── cpm_GENENAME_log2.txt  # cpm values calculated with edgeR, gene id as row index
  └── visual_reports
      └── {species}_{strain}_DEG_{group1}_vs_{group2}_cpmfilterminprop0.75_20190514
          ├── css  # moving or modifying any of these files may disable interactive reports
          ├── images  # moving or modifying any of these files may disable interactive reports
          ├── js  # moving or modifying any of these files may disable interactive reports
          ├── MD-Plot.html  # interactive mean-difference plot (these are linked in the table above)
          └── volcano.html  # interactive volcano plot (these are linked in the table above)
